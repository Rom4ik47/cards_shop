-- auto-generated definition
create table products
(
    id                    int auto_increment
        primary key,
    name                  varchar(50)  not null,
    cost_price            varchar(10)  not null,
    path                  varchar(100) not null,
    product_categories_id int          not null
);

create index partner_id
    on products (product_categories_id);


-- auto-generated definition
create table product_categories
(
    id   int auto_increment
        primary key,
    name varchar(50) not null
);

