<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Магазин игральных карт DKA</title>
    <script>
        if (document.readyState === 'loading' && location.pathname.split('/')[1] === 'admin') {
            let password = prompt("Введите пароль");
            if (password !== "admin") {
                let notice = confirm("Паоль не верен");
                location.href = '/'
            }
        }
    </script>
    <link rel="stylesheet" href="<?= assets('css/app.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/main.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/reset.css') ?>">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="<?= assets('js/axios.min.js') ?>"></script>
    <script src="<?= assets('js/sad_script.js') ?>"></script>
</head>
<body class="shop flex flex_column">
<header class="shop__header header">
    <div class="container header__container flex flex_row">
        <a class="header__phone phone link" href="tel:+79664098682">Тел. +7(996)409-86-82</a>
        <p class="header__shop_name shop_name">DKA</p>
        <a class="header__cart cart flex flex_row" onclick="openNav()">
            <div class="cart__text countProducts"><?= (!empty($_SESSION['basket'])) ? count($_SESSION['basket']) : 0 ?></div>
            <img class="cart__img" src="<?= assets('img/icons/shopping-cart.svg') ?>">
            <p class="cart__text">Корзина</p>
        </a>
    </div>
</header>

<div id='shopNav' class='shop__nav nav'>
    <div class='container-menu'>
        <nav class='navigation'>
            <div class='hamburger-menu flex flex_row'>
                <p>Меню</p>
                <div>
                    <span class='burger-1'></span>
                    <span class='burger-2'></span>
                    <span class='burger-3'></span>
                </div>
            </div>
            <ul class='core-menu'>
                <li>
                    <a class="nav__main_link" href="/">Каталог</a>
                    <ul class='dropdown'>
                        <li>
                            <a class="nav__dropdown_link" href="/">Карты<span class='toggle'></span></a>
                            <ul class='dropdown2'>
                                <li><a class="nav__dropdown_link" href="/cards-magic">Карты для фокусов</a></li>
                                <li><a class="nav__dropdown_link" href="/cards-cardstream">Карты для кардистри</a></li>
                                <li><a class="nav__dropdown_link" href="/cards-poker">Карты для покера</a></li>
                            </ul>
                        </li>
                        <li><a class="nav__dropdown_link" href="/poker-set">Наборы для покера</a></li>
                    </ul>
                </li>
                <li><a class="nav__main_link" href="/">Главная</a></li>
                <li><a class="nav__main_link" href="/about">О нас</a></li>
            </ul>
        </nav>
    </div>
</div>
<?php
echo $data['content'];
?>
<footer>
    <div class="container footer__container flex flex_row">
        <div class="footer__icons">
            <a href="https://vk.com/20dka01" target="_blank">
                <img class="footer__icon" src="<?= assets('img/icons/vk_icon.png') ?>" alt="vk_icon"/>
            </a>
            <a href="https://www.instagram.com/20_d.k.a_01/" target="_blank">
                <img class="footer__icon" src="<?= assets('img/icons/instagram_icon.png') ?>" alt="instagram_icon"/>
            </a>
        </div>
    </div>
</footer>
<div class="shop__basket basket flex flex_column" id="sideCart">
    <a href="javascript:void(0)" class="closeOffcanvas" onclick="closeNav()">×</a>
    <h3 class="basket__title">Ваш заказ:</h3>
    <div class="basket__list list products">
        <?php if (!empty($_SESSION['basket'])) : ?>
            <?php foreach ($_SESSION['basket'] as $item) : ?>
                <div class="list__item item flex flex_row">
                    <span class="item__name"><?= $item['name']; ?></span>
                    <div class="item__counter counter flex flex_row" data-product_id="">
                        <div class="counter__sub" data-product_id="<?= $item['id'] ?>">-</div>
                        <div class="counter__num flex flex_row"><?= $item['count']; ?></div>
                        <div class="counter__add" data-product_id="<?= $item['id'] ?>">+</div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            Пусто
        <?php endif; ?>
    </div>
    <div class="basket__sum sum flex flex_row">
        <p>Сумма</p>
        <div class="sum__count"><?= (!empty($_SESSION['basket'])) ? array_sum(array_column($_SESSION['basket'], 'cost_price')) : 0 ?></div>
        <button class="sum__button" id="orderButton" onclick="popup()">Заказать</button>
    </div>
</div>
<div class="pop-up-wrap flex" style="display:none">
    <div class="pop-up-window">
        <div class="close-popup"></div>
        <form class="pop-up__form flex flex_column" action="/email-send" method="post">
            <input class="pop-up__input" type="text" name="customer_name" required placeholder="Имя Фамилия"/>
            <input class="pop-up__input" type="tel" name="phone" required="" placeholder="Номер телефона">
            <!--            <input class="pop-up__input" type="tel" name="cost_price" placeholder="Итог">-->
            <textarea class="pop-up__textarea" name="message" placeholder="Комментарий"></textarea>
            <input class="pop-up__submit" type="submit" name="Отправить"/>
        </form>
    </div>
</div>

<script src="<?= assets('js/app.js') ?>"></script>
</body>
</html>