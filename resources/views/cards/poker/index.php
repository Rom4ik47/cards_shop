<section class="shop__catalog catalog flex flex_row">
    <?php foreach ($data['getProducts'] as $value) : ?>
        <div class="catalog__goods goods flex flex_column">
            <img class="goods__img" src="<?php echo $value['path'] ?>">
            <p class="goods__name name"><?php echo $value['name'] ?></p>
            <div class="goods__button flex flex_row add_products add_products"  data-product_id="<?php echo $value['id'] ?>">
                <p>В корзину</p>
                <span class="goods__price"><?php echo $value['cost_price'] ?>р</span>
            </div>
        </div>
    <?php endforeach; ?>
</section>