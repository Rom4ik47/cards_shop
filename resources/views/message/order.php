<section class="shop__catalog catalog flex flex_row">
    <form class="admin__add_good add_good" action="/email-send" method="post">
        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Ваше имя</p>
            <input class="add_good__input admin__input" name="customer_name" required placeholder="Введите выше имя"
                   type="text"/>
        </div>
        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Название продукта</p>
            <input class="add_good__input admin__input" name="product_name" placeholder="Название продукта" required
                   type="text"/>
        </div>

        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Моб.телефон</p>
            <input class="add_good__input admin__input" name="phone" placeholder="Введите выш моб телефон" required
                   type="text"/>
        </div>

        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Итого:</p>
            <input class="add_good__input admin__input" name="cost_price" required placeholder="Итоговая цена"
                   type="text"/>
        </div>

        <div class="admin__button_wrap">
            <button class="admin__button_add">Заказать</button>
        </div>
    </form>
</section>

