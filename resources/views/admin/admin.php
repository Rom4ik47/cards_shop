<div class="shop__admin admin flex flex_column">
    <div class="admin__search search">
        <input class="search__input admin__input" type="search" id="search"
               aria-label="Search through site content"/>
        <button class="search__button">Поиск</button>
    </div>
    <form class="admin__add_good add_good" action="/save" enctype="multipart/form-data" method="post">

        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Название товара</p>
            <input class="add_good__input admin__input" name="productName" required
                   type="text"/>
            <select class="add_good__select" name="category_id" required>
                <option selected disabled hidden>Выберете категорию</option>
                <?php foreach ($data['product_categories'] as $value) : ?>
                    <option value="<?= $value['id']; ?>"><?= $value['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Стоимость</p>
            <input class="add_good__input admin__input" name="cost_price_product" required
                   type="number"/>
        </div>
        <div class="add_good__row flex flex__row">
            <p class="add_good__text">Изображение</p>
            <div class="add__image">
                <input class="add_good__input admin__input" name="productImg" type='file' required> <span
                        id='val'></span>
                <span id='button'>Выбрать изображение</span>
            </div>
        </div>

        <div class="admin__button_wrap">
            <button class="admin__button_add">Добавить/изменить</button>
        </div>
    </form>
</div>

<section class="shop__catalog catalog flex flex_row products2">
    <div id="result"></div>
    <?php foreach ($data['products'] as $value) : ?>
        <div class="catalog__goods goods flex flex_column">
            <img class="goods__img" src="<?php echo $value['path'] ?>">
            <p class="goods__name name"><?php echo $value['name'] ?></p>
            <div class="goods__button flex flex_row delete" data-productId="<?php echo $value['id'] ?>">
                <p>Удалить</p>
                <span class="goods__price"><?php echo $value['cost_price'] ?>р</span>
            </div>
            <div class="goods__button flex flex_row edit" onclick="popup_admin(this)"
                 data-productId="<?php echo $value['id'] ?>">
                <p>Изменить</p>
            </div>
        </div>
    <?php endforeach; ?>
</section>
<div class="pop-up-wrap flex_admin" style="display:none">
    <div class="pop-up-window">
        <div class="close-popup"></div>
        <div class="pop-up__form flex flex_column">
            <input class="pop-up__input new_name" type="text" name="new_name" required placeholder="Новое название"/>
            <input class="pop-up__input new_cost" type="text" name="new_cost" required="" placeholder="Новая цена">
            <button class="pop-up__submit edit" type="submit" onclick="">Отправить</button>
        </div>
    </div>
</div>
<script>
    var closePopup = document.querySelector('.close-popup');
    $('.close-popup').click(function (e) {
        e.preventDefault();
        $('.pop-up-wrap').fadeOut();
    });

    function popup_admin(item) {
        let productId = $(item).attr("data-productId");
        localStorage.setItem('id', productId)
        $('.flex_admin').css('display', 'flex')
    }

    $('#button').click(function () {
        $("input[type='file']").trigger('click');
    })
    $("input[type='file']").change(function () {
        $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })

    $(document).ready(() => {
        search();
    })


    $('.delete').click(function () {
        let data = new FormData;
        debugger
        let productId = $(this).attr("data-productId");
        data.append('id', productId)
        if (confirm(`Удалить запись?`)) {
            axios.post('/delete', data).then(
                (resp) => {
                    location.reload();
                },
                (err) => {
                    console.log(err)
                }
            )
        } else {
            return false
        }
    })

    $('.edit').click(function () {
        let name = $('.new_name').val()
        let cost = $('.new_cost').val()
        if (!cost) return
        let productId = localStorage.getItem('id')
        debugger
        let data = new FormData;
        data.append('id', productId)
        data.append('name', name)
        data.append('cost', cost)
        if (!cost) return
        axios.post('/edit', data).then(
            (resp) => {
                location.reload();
            },
            (err) => {
                debugger
                console.log(err)
            }
        )
    })

    function search() {
        $('.search__button').click(() => {
            let value = $('#search').val()
            let data = new FormData;
            data.append('search', value)
            if (!value) return
            axios.post('/admin-search', data).then((resp) => {
                $('.catalog__goods').css('display', 'none')
                if (resp.data.data.length <= 0) {
                    $('#result').css('display', 'block')
                    $('#result').html('<span style="color: white;">Нет результатов</span>')
                } else {
                    $('#result').css('display', 'none')
                }
                resp.data.data.forEach(element => $('.products2').append(`
                    <div class="catalog__goods goods flex flex_column">
                    <img class="goods__img" src=${element.path}>
                                <p class="goods__name name">${element.name}</p>
                                <div class="goods__button flex flex_row delete" data-productId="${element.id}">
                <p>Удалить</p>
                <span class="goods__price">${element.cost_price}р</span>
            </div>
            <div class="goods__button flex flex_row edit" data-productId="${element.id}">
                <p>Изменить</p>
            </div>
                    </div>`))
                del()
            }, (err) => {
                debugger
            })
        })
    }
</script>


