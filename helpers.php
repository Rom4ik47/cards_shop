<?php


if (!function_exists('view')) {
    function view($view, $data = [])
    {
        return \App\Http\View::view($view, $data);
    }
}

if (!function_exists('assets')) {
    function assets($path)
    {
        return '/assets/' . $path;
    }
}

if (!function_exists('env')) {
    function env($key, $default = '')
    {
        if (!file_exists(ROOT_PATH . '/.env')) {
            return die(".env не найден");
        }
        if (empty($_ENV)) {
            foreach (file(ROOT_PATH . '/.env') as $line) {
                $ex = explode('=', trim($line));
                $_ENV[$ex[0]] = $ex[1];
            }
        }
        if (isset($_ENV[$key])) {
            $action = $_ENV[$key];
        } else {
            $action = $default;
        }
        return $action;
    }
}
