<?php
session_start();

use App\Http\Router;

error_reporting(E_ALL);
ini_set("display_errors", 1);

define('ROOT_PATH', dirname(__DIR__));

define('LAYOUTS_PATH', ROOT_PATH . '/resources/layouts/');
define('VIEWS_PATH', ROOT_PATH . '/resources/views/');

require_once __DIR__ . '/../vendor/autoload.php';

Router::init();