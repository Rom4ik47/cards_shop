$('.close-popup').click(function (e) {
    e.preventDefault();
    $('.pop-up-wrap').fadeOut();
});

function popup() {
    $('.flex').css('display', 'flex')
}

function openNav() {
    document.getElementById("sideCart").style.width = "300px";
}

function closeNav() {
    document.getElementById("sideCart").style.width = "0";
}

let bucket = {
    addProduct: function (e) {
        let $this = this;
        let product_id = $($this).data('product_id');
        axios.post('/basket/add', {
            product_id: product_id
        }).then(
            (res) => {
                $('.products').empty();
                for (let item in res.data.basket) {
                    let product = res.data.basket[item];
                    $('.products').append(
                        `
            <div class="list__item item flex flex_row">
                <span class="item__name">${product.name}</span>
                <div class="item__counter counter flex flex_row">
                    <div class="counter__sub" data-product_id="${product.id}">-</div>
                    <div class="counter__num flex flex_row">${product.count}</div>
                    <div class="counter__add" data-product_id="${product.id}">+</div>
                </div>
            </div>`
                    );
                }
                $('.sum__count').text(res.data.sum);
                $('.countProducts').text(res.data.count_basket);
            }, (err) => {
                debugger
            })
    },
    reduceProduct: function (e) {
        let $this = this;
        let product_id = $($this).data('product_id');
        axios.post('/basket/reduce', {
            product_id: product_id
        }).then(
            (res) => {
                $('.products').empty();
                for (let item in res.data.basket) {
                    let product = res.data.basket[item];
                    $('.products').append(
                        `
            <div class="list__item item flex flex_row">
                <span class="item__name">${product.name}</span>
                <div class="item__counter counter flex flex_row">
                    <div class="counter__sub" data-product_id="${product.id}">-</div>
                    <div class="counter__num flex flex_row">${product.count}</div>
                    <div class="counter__add" data-product_id="${product.id}">+</div>
                </div>
            </div>`
                    );
                }
                $('.sum__count').text(res.data.sum);
                $('.countProducts').text(res.data.count_basket);
            }, (err) => {
                debugger
            })
    }
};

// let bucket = {
//     updateLocalStorage: function (item) {
//         let localBucket = localStorage.getItem('bucket');
//         if (!localBucket) {
//             localBucket = "[]";
//         }
//         localBucket = JSON.parse(localBucket)
//         localBucket.push(item)
//         localStorage.setItem("bucket", JSON.stringify(localBucket))
//
//         bucket.render()
//     },
//     addProduct: function (e) {
//         let item = JSON.parse($(this).attr('data-product'));
//         bucket.updateLocalStorage(item)
//     },
//     render: function () {
//
//     }
// };

$('.add_products').on('click', bucket.addProduct);

$(document).on('click', '.counter__add', bucket.addProduct);
$(document).on('click', '.counter__sub', bucket.reduceProduct);