Vue.config.productionTip = false;
Vue.config.devtools = false;

const app = new Vue({
    el: '#app',
    data: {
        file: undefined,
        settings: [],
        radio: undefined,
        isLoading: false,
        form: undefined,
        div_er: true,
        error: 'Выберете нужную пп',
        partners: [],
        disabled: false,
    },
    mounted() {
        this.loadData()
    },
    methods: {
        loadData() {
            axios.get('/get-partners').then((res) => {
                if (res.data.length === 0) {
                    this.disabled = true
                    return
                }
                this.partners = res.data
            })
        },
        snackbar() {
            this.$buefy.snackbar.open(`Default, positioned bottom-right with a green 'OK' button`)
        },
        danger() {
            this.$buefy.snackbar.open({
                duration: 5000,
                message: 'Snackbar with red action, positioned on bottom-left and a callback.<br>Note: <em>Message can include html</em>.',
                type: 'is-danger',
                position: 'is-bottom-left',
                actionText: 'Undo',
                queue: false,
            })
        },
        sendFile() {
            if (this.form === '') return;
            if (!this.file) return;
            this.isLoading = true;
            let formData = new FormData();
            formData.append('file', this.file)
            formData.append('form', this.form)
            axios.post('/api/convert-file', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(
                (resp) => {
                    this.isLoading = false
                    if (resp.data.url) {
                        window.open(resp.data.url + '?file=' + resp.data.file)
                    }
                },
                (err) => {
                    this.isLoading = false
                    alert(err.message)
                }
            )
        }
    }
})
