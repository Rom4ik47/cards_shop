$(document).ready(function(){
    $(".core-menu li").hover(
        function(){
            $(this).children('ul').slideDown('fast');
        },

        function () {
            $('ul', this).slideUp('fast');
        });

    $(".hamburger-menu").click(function(){
        $(".burger-1, .burger-2, .burger-3").toggleClass("open");
        $(".core-menu").slideToggle("fast");
    });
});

