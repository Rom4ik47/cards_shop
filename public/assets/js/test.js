$('.add_products').click(function () {
    if (localStorage.length == 0) {
        let arr = [];
        let obj = {}

        let productId = $(this).attr("data-productId");
        let productPrice = $(this).attr("data-productPrice");
        let productName = $(this).attr("data-productName");
        obj = {
            productId: productId,
            productPrice: productPrice,
            productName: productName,
            count: 1
        }
        arr.push(obj)
        sessionStorage.setItem("basket", JSON.stringify(arr));
        $(this).html('<p>Товар добавлен в корзину</p>')
    }
    $(this).html('<p>Товар в корзине</p>')
})


function getLocalStorage(getValue) {
    let array = []
    let storedNames = JSON.parse(localStorage.getItem("products"));
    for (let key in storedNames) {
        array.push(storedNames[key][getValue])
    }
    return array.join(',')
}


function setIdProduct(item) {
    let productId = $(item).attr("data-productId");
    localStorage.setItem('id', productId)
}


$(document).ready(function () {
    let storedNames = JSON.parse(localStorage.getItem("products"))
    if (storedNames.length > 0) {
        storedNames.sort()
        let getElement = document.getElementsByClassName('goods__button')
        let count = 0
        for (let i of getElement) {
            let productId = parseInt($(i).attr("data-productId"))
            if (storedNames[count] == productId) {
                count++
                $(i).html('<p>Товар уже добавлен в корзину</p>')
            }
        }
    }
})