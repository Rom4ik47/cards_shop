<?php

namespace App;

class Mysql
{
    private $connect;

    public function __construct()
    {
        if (!$this->connect) {
            $this->connect = mysqli_connect(env('DB_CONNECTION'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE'));
            if (!$this->connect) {
                header("HTTP/1.0 500 Internal Server Error");
                die("Failed to connect to MySQL: " . $this->connect->connect_error);
            }
        }
    }


    public function query($sql)
    {
        $result = $this->connect->query($sql);
        if (!$result) {
            header("HTTP/1.0 500 Internal Server Error");
            die("Mysql error: " . $this->connect->error);
        }
        return is_bool($result) ? $result : $result->fetch_all(MYSQLI_ASSOC);
    }

    public function first($sql)
    {
        $query = $this->query($sql);
        if(!$query) {
            return [];
        }
        return $query[0];
    }

}