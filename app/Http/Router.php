<?php

namespace App\Http;

use App\Http\Controllers\AboutController;
use App\Http\Controllers\BasketController;
use App\Http\Controllers\CardsMagicController;
use App\Http\Controllers\CardsPokerController;
use App\Http\Controllers\CardsPokerSetController;
use App\Http\Controllers\CardsStreamController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReplaceController;


class Router
{
    private static $routes = [
        '/' => [ReplaceController::class, 'getProducts', 'GET'],
//        '/get-products' => [ReplaceController::class, 'getProducts', 'GET'],
        '/email' => [EmailController::class, 'index', 'GET'],
        '/email-send' => [EmailController::class, 'test', 'POST'],

        '/save' => [ProductController::class, 'addProduct', 'POST'],
        '/delete' => [ProductController::class, 'delete', 'POST'],
        '/edit' => [ProductController::class, 'edit', 'POST'],
        '/getProductCategories' => [ReplaceController::class, 'getProductCategories', 'GET'],
        '/admin' => [ProductController::class, 'index', 'GET'],
        '/admin-search' => [ProductController::class, 'search', 'POST'],

        '/basket' => [BasketController::class, 'index', 'POST'],
        '/basket/add' => [BasketController::class, 'add', 'POST'],
        '/basket/reduce' => [BasketController::class, 'reduce', 'POST'],
        '/basket/get' => [BasketController::class, 'get', 'GET'],

        '/about' => [AboutController::class, 'index', 'GET'],
        '/cards-cardstream' => [CardsStreamController::class, 'index', 'GET'],
        '/cards-poker' => [CardsPokerController::class, 'index', 'GET'],
        '/cards-magic' => [CardsMagicController::class, 'index', 'GET'],
        '/poker-set' => [CardsPokerSetController::class, 'index', 'GET'],
    ];

    static function init()
    {
        $request_url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
        $request_method = $_SERVER['REQUEST_METHOD'];
        foreach (self::$routes as $route => $controller) {
            if ($request_url == $route) {
                if (!class_exists($controller[0])) {
                    echo "Класс " . $controller[0] . " не найден.";
                    return [];
                }
                if (!method_exists($controller[0], $controller[1])) {
                    echo "Метод " . $controller[1] . " в " . $controller[0] . " не найден.";
                    return [];
                }
                if (strtoupper($request_method) == strtoupper($controller[2])) {
                    $class = new $controller[0];
                    $method = $controller[1];
                    $request_data = [];
                    if ($request_method == 'POST') {
                        if ($_SERVER['CONTENT_TYPE'] && $_SERVER['CONTENT_TYPE'] == 'application/json;charset=UTF-8') {
                            $request_data = json_decode(file_get_contents('php://input'), true);
                        } else {
                            $request_data = $_POST;
                        }
                    }
                    if ($request_method == 'GET') {
                        $request_data = $_GET;
                    }
                    if ($request_method == 'DELETE') {
                        $request_data = $_GET;
                    }
                    if ($request_method == 'PUT') {
                        $request_data = $_POST;
                    }
                    return $class->$method(new Request($request_data, $_FILES));
                }
            }
        }
        header("HTTP/1.0 404 Not Found");
        return [];
    }
}