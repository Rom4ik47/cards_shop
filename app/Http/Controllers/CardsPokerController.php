<?php

namespace App\Http\Controllers;
use App\Mysql;

class CardsPokerController
{
    public function index()
    {
        $getProducts = (new Mysql())->query("SELECT * FROM products WHERE product_categories_id=3");

        return view('cards/poker/set/index', [
            'getProducts' => $getProducts
        ]);
    }
}