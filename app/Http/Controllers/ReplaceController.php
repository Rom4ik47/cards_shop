<?php

namespace App\Http\Controllers;

use App\Mysql;

class ReplaceController
{

    public function index()
    {
        return view('index');
    }

    public function getProducts()
    {
        $products = (new Mysql())->query('SELECT * FROM products');
        return view('index', [
            'products' => $products,
        ]);
    }
}