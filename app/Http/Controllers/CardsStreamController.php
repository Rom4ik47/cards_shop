<?php

namespace App\Http\Controllers;

use App\Mysql;

class CardsStreamController
{
    public function index()
    {
        $getProducts = (new Mysql())->query("SELECT * FROM products WHERE product_categories_id=2");

        return view('cards/cardstream/index', [
            'getProducts' => $getProducts
        ]);
    }
}