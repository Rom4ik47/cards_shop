<?php

namespace App\Http\Controllers;

use App\Http\Request;
use App\Mysql;

class BasketController
{
    public function add(Request $request)
    {
        if (!$request->exist('product_id')) {
            echo json_encode([
                'error' => 'Укажите ID товара'
            ]);
            return;
        }
        $product = (new Mysql())->first("SELECT * FROM products WHERE id = '" . $request->get('product_id') . "'");
        if (empty($product)) {
            echo json_encode([
                'error' => 'Товар не найден'
            ]);
            return;
        }
        if (empty($_SESSION['basket'])) {
            $_SESSION['basket'] = [];
        }
        $result = [
            'name' => $product['name'],
            'id' => $product['id'],
            'cost_price' => $product['cost_price'],
            'count' => 1,
        ];
        if (array_key_exists($product['id'], $_SESSION['basket'])) {
            $_SESSION['basket'][$product['id']]['count'] += 1;
            $_SESSION['basket'][$product['id']]['sum'] = $product['cost_price'] * $_SESSION['basket'][$product['id']]['count'];
        } else {
            $_SESSION['basket'][$product['id']] = $result;
            $_SESSION['basket'][$product['id']]['sum'] = $product['cost_price'];
        }
        echo json_encode([
            'basket' => $_SESSION['basket'],
            'sum' => array_sum(array_column($_SESSION['basket'], 'sum')),
            'count_basket' => count($_SESSION['basket'])
        ]);
    }


    public function reduce(Request $request)
    {
        $product_id = $request->get('product_id');
        if (!$product_id) {
            return [
                'error' => 'Укажите ID товара'
            ];
        }
        $product = (new Mysql())->first("SELECT * FROM products WHERE id = '" . $request->get('product_id') . "'");
        if (empty($product)) {
            echo json_encode([
                'error' => 'Товар не найден'
            ]);
            return;
        }
        if (array_key_exists($product['id'], $_SESSION['basket'])) {
            if ($_SESSION['basket'][$product['id']]['count'] > 1) {
                $_SESSION['basket'][$product['id']]['count'] -= 1;
                $_SESSION['basket'][$product['id']]['sum'] = $product['cost_price'] * $_SESSION['basket'][$product['id']]['count'];
            } else {
                unset($_SESSION['basket'][$product['id']]);
            }
        }
        echo json_encode([
            'basket' => $_SESSION['basket'],
            'sum' => array_sum(array_column($_SESSION['basket'], 'sum')),
            'count_basket' => count($_SESSION['basket'])
        ]);
    }

    public function index(Request $request)
    {
        $id = $request->get('id');
        $getPath = (new Mysql())->query("SELECT * from products  where id in ({$id})");
        echo json_encode(['data' => $getPath]);
    }
}