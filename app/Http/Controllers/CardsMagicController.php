<?php

namespace App\Http\Controllers;

use App\Mysql;

class CardsMagicController
{
    public function index()
    {
        $getProducts = (new Mysql())->query("SELECT * FROM products WHERE product_categories_id=1");

        return view('cards/magic/index', [
            'getProducts' => $getProducts
        ]);
    }
}