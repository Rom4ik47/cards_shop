<?php

namespace App\Http\Controllers;

use App\Http\Request;

class EmailController
{
    public function index()
    {
        return view('message/order');
    }

    public function test(Request $request)
    {
        $name = $request->get('customer_name');
        $phone = $request->get('phone');
        $comment = $request->get('message');


        $products = "";
        foreach ($_SESSION['basket'] as $product) {
            $products .= $product['name'] . "({$product['count']} шт.)" . PHP_EOL;
        }

        $sum = array_sum(array_column($_SESSION['basket'], 'sum'));

        $params = [
            'chat_id' => env('TELEGRAMM_USER_ID'),
            'text' => 'номер телефона клиента: ' . $phone . PHP_EOL . 'имя клиента: ' . $name
                . PHP_EOL . 'Товары: ' . PHP_EOL . $products . PHP_EOL . 'На сумму: ' . $sum . ' руб.' . PHP_EOL . 'Комментарий: ' . $comment
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot' . env('TELEGRAMM_BOT_ID') . '/sendMessage'); // адрес api телеграмм
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_exec($curl);
        curl_close($curl);
        unset($_SESSION['basket']);
        header('location: /');
    }
}