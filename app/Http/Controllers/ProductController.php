<?php

namespace App\Http\Controllers;

use App\Http\Request;
use App\Mysql;

class ProductController
{
    public function index()
    {
        $product_categories = (new Mysql())->query('SELECT * FROM product_categories');
        $products = (new Mysql())->query('SELECT * FROM products');

        return view('admin/admin', [
            'product_categories' => $product_categories,
            'products' => $products
        ]);
    }


    public function addProduct(Request $request)
    {
        $file = $request->file('productImg');
        $productName = $request->get('productName');
        $category_id = $request->get('category_id');
        $cost_price_product = $request->get('cost_price_product');
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $newName = uniqid() . '.' . $ext;

        $target = 'storage/' . $newName;

        $res = move_uploaded_file($file['tmp_name'], $target);
        if ($res) {
            (new Mysql())->query("INSERT INTO products (`name`, `cost_price`,`path`,`product_categories_id`)
values ('{$productName}','{$cost_price_product}','{$target}','{$category_id}')");
        }
        header('location: /admin');
    }


    public function edit(Request $request)
    {
        $name = $request->get('name');
        $cost_price = $request->get('cost');
        $id = $request->get('id');
        (new Mysql())->query("UPDATE products SET `name`='{$name}',`cost_price`='{$cost_price}' WHERE id='{$id}'");
        echo json_encode([
            'data' => true
        ]);
    }

    public function search(Request $request)
    {
        $like = $request->get('search');

        $products = (new Mysql())->query("SELECT * from products WHERE name LIKE '%{$like}%'");
        echo json_encode([
            'data' => $products
        ]);
    }

    public function delete(Request $request)
    {
        $idProduct = $request->get('id');
        $getPath = (new Mysql())->first("SELECT path from products where id= '{$idProduct}'");
        (new Mysql())->query("DELETE  from products where id= '{$idProduct}'");
        unlink($getPath['path']);
    }
}